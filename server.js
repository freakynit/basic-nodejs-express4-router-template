var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mime = require('mime');
var fs = require('fs');

var app = express();

var PORT = 3339;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret : "SHUUUUSH",
    saveUninitialized: true,
    resave : false 
}));

var index = require('./routes/index');
app.use(index);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            "message": err.message,
            "error": err
        });
    });
}

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        "message": err.message,
        "error": {}
    });
});

app.listen(PORT, function(){
  console.log("Started on PORT " + PORT);
});
